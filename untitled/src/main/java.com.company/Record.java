package main.java.com.company;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by heinr on 28.10.2016.
 */
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;

    private String firstName;

    transient private String secondName;

    private String surname;
    private Long number;
    private String fullName;
    private String phoneNumber;
    private String address;

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public Long getNumber() {
        return number;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "First name '" + firstName + '\'' +
                ", Second name '" + secondName + '\'' +
                ", Surname '" + surname + '\'' +
                ", number " + number +
                ", phone number '" + phoneNumber + '\'' +
                ", address '" + address + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Record)) return false;

        Record record = (Record) o;

        if (!getFullName().equals(record.getFullName())) return false;
        if (getPhoneNumber() != null ? !getPhoneNumber().equals(record.getPhoneNumber()) : record.getPhoneNumber() != null)
            return false;
        if (getAddress() != null ? !getAddress().equals(record.getAddress()) : record.getAddress() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getFullName().hashCode();
        result = 31 * result + (getPhoneNumber() != null ? getPhoneNumber().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        return result;
    }

    public void createFullName() {
        this.fullName = this.surname + " " + this.firstName + " " + this.secondName;
    }

    private void setFields() {
        String[] arrayOfNames = this.fullName.split(" ");
        this.surname = arrayOfNames[0];
        this.firstName = arrayOfNames[1];
        this.secondName = arrayOfNames[2];
    }

    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        if(fullName == null) createFullName();
        out.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {

        in.defaultReadObject();

        //Заполнение SecondName
        String[] arrayOfNames = this.fullName.split(" ");
        this.secondName = arrayOfNames[2];
    }
}