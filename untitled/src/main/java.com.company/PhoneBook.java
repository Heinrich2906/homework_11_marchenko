package main.java.com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 31.10.2016.
 */
public class PhoneBook implements Serializable {

    private List<Record> records = new ArrayList<>(); // список записей в книге

    private static final long serialVersionUID = 2L; // для сериализации

    public List<Record> getRecords() {
        return records;
    }

    @Override
    public String toString() {

        boolean firstRun;
        firstRun = true;

        String result = "";
        String prefix;

        for (Record s : records) {

            if (firstRun) {
                firstRun = false;
                prefix = "";
            } else
                prefix = ";";

            result = result + prefix + s.toString();
        }

        return result;
    }

    public void addRecord(Record record) {
        int length = records.size();
        long longLength = (long) (length + 1);
        record.setNumber(longLength);
        records.add(record);
    }

    public void removeRecord(Record record) {

        records.remove(record);

        long i = 0L;

        for (Record s : records) {
            if (s.getNumber() != i)
                s.setNumber(i);
            i = i + 1;
        }

    }

    public boolean contains(Record record) {
        return records.contains(record);
    }

    Record findByNumber(Long number) {

        Record result = null;

        for (Record s : records) {
            if (s.getNumber() == number) {
                result = s;
                break;
            }
        }

        return result;
    }

    List<Record> findByFullName(String name) {

        List<Record> result = new ArrayList<>();

        for (Record s : records) {
            if (s.getFullName().contains(name.trim())) result.add(s);
        }

        return result;
    }

    List<Record> findByPhoneNumber(String phoneNumber) {

        List<Record> result = new ArrayList<>();

        for (Record s : records) {
            if (s.getPhoneNumber() == phoneNumber) {
                result.add(s);
                break;
            }
        }

        return result;
    }

    List<Record> findByAddress(String address) {

        List<Record> result = new ArrayList<>();

        for (Record s : records) {
            if (s.getAddress().contains(address)) result.add(s);
        }

        return result;
    }

    public void writeObject(java.io.ObjectOutputStream out) throws IOException {

        for (Record s : records) {
            out.writeObject(s);
        }

        out.close();
    }

    public void readObject(java.io.FileInputStream fis) throws IOException, ClassNotFoundException {

        ObjectInputStream ois = new ObjectInputStream(fis);

        while (fis.available() > 0) records.add((Record) ois.readObject());

        ois.close();
    }
}