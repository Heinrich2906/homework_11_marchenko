package main.java.com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 31.10.2016.
 */
public class Main {

    private static final String file = "phoneBook.txt";
    private static String menuChiose = new String();
    static PhoneBook phoneBook;

    public static void main(String[] args) throws Exception {

        phoneBook = new PhoneBook();
        Main main = new Main();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        phoneBook = main.readFromFile();

        System.out.println("--- Start of the program ---");

        while (menuChiose != "exit") {

            System.out.print("Enter the operation name: ");
            menuChiose = reader.readLine();

            switch (menuChiose) {
                case "add":
                    add();
                    break;
                case "print":
                    printBook();
                    break;
                case "update":
                    updateRecord(reader);
                    break;
                case "remove":
                    updateRecord(reader);
                    break;
                case "find":
                    searchInBook(reader);
                    break;
                case "exit":
                    writeToFile();
                    break;
                default:
                    printBook();
            }
        }
    }

    public static void writeToFile() throws Exception {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        phoneBook.writeObject(oos);
    }

    public static PhoneBook readFromFile() throws Exception {

        FileInputStream fis = new FileInputStream(file);
        phoneBook.readObject(fis);

        return phoneBook;

    }

    public static void add() throws Exception {

        String newLine;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            do {

                Record record = new Record();

                System.out.print("Enter a First name: ");
                newLine = reader.readLine();
                if (newLine.equals("exit")) break;
                else record.setFirstName(newLine);

                System.out.print("Enter a Second name: ");
                newLine = reader.readLine();
                if (newLine.equals("exit")) break;
                else record.setSecondName(newLine);

                System.out.print("Enter a Surname: ");
                newLine = reader.readLine();
                if (newLine.equals("exit")) break;
                else record.setSurname(newLine);

                System.out.print("Enter a Address: ");
                newLine = reader.readLine();
                if (newLine.equals("exit")) break;
                else record.setAddress(newLine);

                System.out.print("Enter a Phone number: ");
                newLine = reader.readLine();
                if (newLine.equals("exit")) break;
                else record.setPhoneNumber(newLine);

                record.createFullName();

                phoneBook.addRecord(record);

            } while (!newLine.equals("exit"));

            menuChiose = "";

        } catch (IOException ex) {
            System.out.println("Reading error");
        }
    }

    public static void printBook() throws Exception {
        for (Record s : phoneBook.getRecords()) {
            System.out.println(s.toString());
        }
    }

    public static void updateRecord(BufferedReader reader) throws IOException {

        System.out.print("Update record with number: ");
        String noteNumber = reader.readLine();
        Long myDigit = 0L;
        String newLine;
        Record record;

        try {
            myDigit = Long.parseLong(noteNumber.trim());
        } catch (NumberFormatException ex) {

            System.out.println("не получается извлечь число из строки");
            return;
        }

        record = phoneBook.findByNumber(myDigit);

        if (record != null) {

            System.out.print("Enter New First name: ");
            newLine = reader.readLine();
            if (newLine.equals("exit")) return;
            if (!newLine.trim().equals("")) record.setFirstName(newLine);

            System.out.print("Enter New Second name: ");
            newLine = reader.readLine();
            if (newLine.equals("exit")) {
                record.createFullName();
                return;
            }
            if (!newLine.trim().equals("")) record.setSecondName(newLine);

            System.out.print("Enter New Surname: ");
            newLine = reader.readLine();
            if (newLine.equals("exit")) {
                record.createFullName();
                return;
            }
            if (!newLine.equals("")) record.setSurname(newLine);

            System.out.print("Enter New Address: ");
            newLine = reader.readLine();
            if (newLine.equals("exit")) {
                record.createFullName();
                return;
            }
            if (!newLine.trim().equals("")) record.setAddress(newLine);

            System.out.print("Enter New Phone number: ");
            newLine = reader.readLine();
            if (newLine.equals("exit")) {
                record.createFullName();
                return;
            }
            if (!newLine.trim().equals("")) record.setPhoneNumber(newLine);

            record.createFullName();
        } else System.out.println("--- No record with such number! ---");
    }

    public static void removeRecord(BufferedReader reader) throws IOException {

        System.out.print("Remove record with number: ");
        String noteNumber = reader.readLine();
        Long myDigit = 0L;
        Record record;

        try {
            myDigit = Long.parseLong(noteNumber.trim());
        } catch (NumberFormatException ex) {

            System.out.println("не получается извлечь число из строки");
            return;
        }
        record = phoneBook.findByNumber(myDigit);

        if (record != null) {
            phoneBook.removeRecord(record);
        } else System.out.println("--- No record with such number! ---");

    }

    public static void searchInBook(BufferedReader reader) throws IOException {

        System.out.print("Find record by (1. Name, 2. PhoneNumber, 3. Address):");
        List<Record> searchList = new ArrayList<Record>();
        String searchString = reader.readLine();
        String fileName = new String();
        Long myDigit = 0L;
        Record record;

        switch (searchString) {
            case "1":
                System.out.print("Enter a (part of) Name:");
                searchString = reader.readLine();
                searchList = phoneBook.findByFullName(searchString);
                fileName = "SearchByName.txt";
                break;
            case "2":
                System.out.print("Enter a Phone number:");
                searchString = reader.readLine();
                searchList = phoneBook.findByPhoneNumber(searchString);
                fileName = "SearchByPhone.txt";
            case "3":
                System.out.print("Enter an Address:");
                searchString = reader.readLine();
                searchList = phoneBook.findByAddress(searchString);
                fileName = "SearchByAddress.txt";
            default:
                return;
        }

        if (searchList.size() == 0) {
            System.out.println("Find nothing");
            return;
        } else {
            for (Record s : searchList) {
                System.out.println(s.toString());
            }
            System.out.print("Save the results?");

            searchString = reader.readLine();

            if (searchString.trim().equals("y")) {

                FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream oos = new ObjectOutputStream(fos);

                for (Record s : searchList) {
                    oos.writeObject(s);
                }

                oos.close();
            }
        }
    }
}
